select * from shippers;

select orders.shipperid, count(orderid)
from orders join shippers on orders.shipperid=shippers.shipperid
group by orders.shipperid;


# bizden en az bir kez sipariş veren müşterileri istiyoruz.(inner join)
select customername,orderid
from customers join orders on orders.customerid=customers.customerid
order by orderid;

# hala sipariş vermemiş müşterilerin isimlerini de görebiliriz left join yaparsak
select customername,orderid
from customers left join orders on orders.customerid=customers.customerid
order by orderid;

# her siparişi hazırlayan işçilerin isimlerini alalım
select firstname,lastname,orderid
from orders join employees on orders.employeeid = employees.Employeeid
order by orderid;

# bu sefer hiç sipariş hazırlamayan işçilerin isimlerini de alalım (right join)
select firstname,lastname,orderid
from orders right join employees on orders.employeeid = employees.Employeeid
order by orderid;



