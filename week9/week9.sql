select * from customers;

update customers set country=replace(country,"\n", '');
update customers set city=replace(country,"\n", '');

create view mexicanCustomers as 
select customerid,customername,contactnamemexicancustomers
from customers 
where country="Mexico";

select * from mexicancustomers;

select *
from mexicancustomers join orders on mexicancustomers.customerid = orders.customerid;

create view productsbelowavg as 
select productid,productname,price
from products 
where price < (select avg(price) from  products);

#truncate is faster than delete
#delete from orderdetails where productid=5;
#truncate orderdetails;

#delete from customers;  Hata verir.Çünkü customers tablosunda foreign key var.İçi doğrudan boşaltılamaz.
#delete from orders;   Bu da hata verir.Aynı sebepten dolayı

#drop table customers;
 

